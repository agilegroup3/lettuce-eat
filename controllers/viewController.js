const path = require('path')


// LOG IN PAGE
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname,'../','views','login.html'))
}

exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname,'../','views','signup.html'))
}

exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname,'../','views','index.html'))
}

exports.getAdmin = (req, res) => {
    res.sendFile(path.join(__dirname,'../','views','admin.html'))
}

