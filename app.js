const express = require("express")
const path = require('path')
const app = express()


// const port = 4000
const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')

app.use(express.json())
app.use('/api/v1/users',userRouter)

app.use('/',viewRouter)

app.use(express.static(path.join(__dirname,'views')))
module.exports = app

// app.listen(port, () => {
//     console.log('App runnig on port ${port} ..')
// })